const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");

//Route for checking if the user's email is already existing in our database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/details", (req, res) => {
	userController.getProfile(id: req.body.id).then(resultFromController => res.send(resultFromController))
})

module.exports = router;