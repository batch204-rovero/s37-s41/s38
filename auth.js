const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

//JSON Web Tokens

//Token Creation
module.exports.createAccessToken = (user) => {
	console.log(user)
	/*
		{
		  _id: new ObjectId("63401354f69e24abfb9bcaef"),
		  firstName: 'Aron',
		  lastName: 'Rovero',
		  email: 'aronrovero@mail.com',
		  password: '$2b$10$vIQwfDhVK/DCYF5/RzruGenReNV6M3EVgxeoVZWCx7/3YD4U2v/vO',
		  isAdmin: false,
		  mobileNo: '09171234567',
		  enrollments: [],
		  __v: 0
		}

	*/
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//Generate a token
		//jwt.sign(<payload>)
	return jwt.sign(data, secret, {expiresIn: "2h"})
}