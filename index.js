const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const details = require("./routes/detailRoutes");

const port = 4000;

const app = express();

//Connect application to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin123@batch204-roveroaron.k3oycwf.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

app.use(cors());
app.use(express.json());

//localhost:4000/users/checkEmail
app.use("/users", userRoutes)

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});